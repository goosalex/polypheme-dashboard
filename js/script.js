$(document).ready(function() {



    //sort table by col
    $('th span:nth-child(1)').click(function() {
        var table = $(this).parents('table').eq(0)
        var rows = table.find('tr:gt(0)').toArray().sort(comparer($(this).index()))
        this.asc = !this.asc
        if (!this.asc) { rows = rows.reverse() }
        for (var i = 0; i < rows.length; i++) { table.append(rows[i]) }
    })

    function comparer(index) {
        return function(a, b) {
            var valA = getCellValue(a, index),
                valB = getCellValue(b, index)
            return $.isNumeric(valA) && $.isNumeric(valB) ? valA - valB : valA.toString().localeCompare(valB)
        }
    }

    function getCellValue(row, index) { return $(row).children('td').eq(index).text() }






    //copy in clipboard
    $("article div.id").click(function() {

        var text = $(this).data('id');
        var $this = $(this);
        var $input = $('<input type=text>');
        $input.prop('value', text);
        $input.insertAfter($(this));
        $input.focus();
        $input.select();
        $this.hide();

        document.execCommand("copy");
        $this.show();
        $input.remove();
        toaster("ID copied into clipboard")

    });


    $('table th span:nth-child(2)').click(function() {
        var cell = $(this).closest('th');
        var col = cell[0].cellIndex + 1;
        $("th:nth-child(" + col + ")").css("display", "none");
        $("td:nth-child(" + col + ")").css("display", "none");
    });



    //silent notification
    function toaster(text) {

        $("div#toast").text(text)
        $("div#toast").animate({ top: "0px" }, 1000, "linear", function() {


            $("div#toast").delay(2000).animate({ top: "-100px" }, 1000);


        });

    }






    //table 2 csv

    $('#download').on('click', function() {
        $('table').table2csv({
            file_name: 'export.csv',
            header_body_space: 0
        });
    })





    $('.grid').masonry({
        // options
        itemSelector: '.grid-item'
    });



})