<?php include ("oauth-session.php") ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="icon" type="image/png" href="favicon.png">
    <meta charset="utf-8">
    <meta name="description" content="Dashboard">
    <title>Dashboard</title>
    <link rel="stylesheet" href="css/main.css">
    <link rel="stylesheet" href="css/grid.css">

    <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">
</head>




<body id="stats">
<?php include("navigation.php") ?>


  <div id='wrapper' class="grid">
    <div id="toast"></div>


    <?php

    //get url/user/pw
    require("config.php");

    $salary_array = [];
    $salary_updated = "--";
    $sales_updated = "--";




    $newDate = new DateTime();
    $now = $newDate->getTimestamp(); 

    $today = $newDate->format('d.m.Y');
    $today_month = $newDate->format('n');
    $day = "";
    $year =  $newDate->format('Y');



    ///////////////////
    //fetch data salary
    ///////////////////

    $string = file_get_contents($url_salary);
    $json_salary = json_decode($string, true);

    if(!empty($json_salary["values"]))
    {

        foreach($json_salary["values"] as $key=>$value){
        if($key == 0)
            {
                $salary_updated = $value[3];
            }


                if($value[1] == $year)
                {
                    array_push($salary_array,["year"=>$value[1],"salary"=>$value[0]]);
                
                }
            }
    }



    ///////////////////
    //fetch data sales
    ///////////////////


    
        $context = stream_context_create(array(
            'http' => array(
                'header'  => "Authorization: Basic " . base64_encode($user.":".$pass)
            )
        ));
        $data = file_get_contents($url,true,$context);
        $json_a = json_decode($data,true);





        $total_year;
        $month_array =[0,0,0,0,0,0,0,0,0,0,0,0];
        $data_array =[[
                "timestamp"=>0 ,
                "date"=>0,
                "datetime"=>0,
                "sum"=>0,
                "reference"=>0,
                "stop"=>0,
                "orderedBy"=>0,
                "pick"=>0,
                "drop"=>0,
                "status"=>0
        ]
        ];




        foreach($json_a as $key=>$value)
        {
            $startDate_unix = strtotime($json_a[$key]["meta.summaries.startDate"]);
            $date = new DateTime($json_a[$key]["meta.summaries.startDate"]);
            $date_before = new DateTime($json_a[$key-1]["meta.summaries.startDate"]);
            $datetime = new DateTime($json_a[$key]["meta.summaries.startDate"].$json_a[$key]["meta.summaries.startTime"] );
            $reference = $json_a[$key]["reference"];
            $pick = $json_a[$key]["meta.summaries.startShortInfo"];
            $drop = $json_a[$key]["meta.summaries.endShortInfo"];
            $orderedBy = $json_a[$key]["orderedByCustomerId"];
            $reference = $json_a[$key]["reference"];
            $status =  $json_a[$key]["status"];
            $price = floor($json_a[$key]["sums.base"]);
            $status = $json_a[$key]["status"];
            $stop = $json_a[$key]["meta.summaries.stepsShortInfo"];




            if(is_numeric($price) && $startDate_unix > 0)
            {
            if($status == "produced" || $status == "billed"  || $status == "ordered")
            {

            //push in array to sort 

                array_push($data_array,[
                    "timestamp"=> $startDate_unix ,
                    "date"=>$date->format('d.m.Y'),
                    "datetime"=>$datetime,
                    "sum"=>$price,
                    "reference"=>$reference,
                    "stop"=>$stop,
                    "orderedBy"=>$orderedBy,
                    "pick"=>$pick,
                    "drop"=>$drop,
                    "status"=>$status
                    ]);


                //sum total year
                $total_year = $price+$total_year; 

                //sum months
                $month = $datetime->format('m');

               
                if($month == 1)
                {
                    $month_array[0] = $price+$month_array[0];
                }

                if($month == 2)
                {
                    $month_array[1] = $price+$month_array[1];
                }

                if($month == 3)
                {
                    $month_array[2] = $price+$month_array[2];
                }

                if($month == 4)
                {
                    $month_array[3] = $price+$month_array[3];
                }
                if($month == 5)
                {
                    $month_array[4] = $price+$month_array[4];
                }
                if($month == 6)
                {
                    $month_array[5] = $price+$month_array[5];
                }
                if($month == 7)
                {
                    $month_array[6] = $price+$month_array[6];
                }
                if($month == 8)
                {
                    $month_array[7] = $price+$month_array[7];
                }
                if($month == 9)
                {
                    $month_array[8] = $price+$month_array[8];
                }
                if($month == 10)
                {
                    $month_array[9] = $price+$month_array[9];
                }
                if($month == 11)
                {
                    $month_array[10] = $price+$month_array[10];
                }
                if($month == 12)
                {
                    $month_array[11] = $price+$month_array[11];
                }

                

            }
        }
    }


        //before getting sum per day sort array by date
        function mySort($a,$b) {
            
                return $a['timestamp'] - $b['timestamp'];
            
        }
        usort($data_array, 'mySort'); 

        /*
        echo '<pre>';
        echo print_r($data_array);
        echo '<pre>';
        */
        



        $highest_key;
        $highest_value = 0;
        $highest_date = 0;
        $sum_today;

        echo '<article id="days">';
        foreach ($data_array as $key => $value) {
            $sum =+ $data_array[$key]["sum"]+$sum;

            if($data_array[$key] > 1)
            {
            if($data_array[$key]["timestamp"] != $data_array[$key-1]["timestamp"])
            {
                echo "<div><span>".$data_array[$key-1]["date"]."</span>".$sum."</div>";
                
                //get highest sum
                if ($sum > $highest_value) {
                    $highest_value = $sum;
                    $highest_date = $data_array[$key-1]["date"];
                }
                if($data_array[$key]["date"] == $today)
                {
                    $sum_today = $sum;
                }


                $sum = 0;

            
        }
        }
    }
        echo '</article>';

?>






    <div class="grid-col-30 grid-item" id="info-box">
        <article id="today">
            <h1>Today</h1>
            <div><?php echo round($sum_today,2) ?> CHF</div>
        </article>
        <article id="month">
            <h1>Month</h1>
            <div><?php echo round($month_array[3-1],2); ?> </div>
        </article>
        <article>
            <h1>Year</h1>
            <div><?php echo round($total_year,2); ?> CHF</div>
        </article>
        <article>
            <h1>Best Day of the Year</h1>
            <div><?php echo round($highest_value,2); ?> CHF / <?php echo $highest_date; ?></div>
        </article>

    <article id="month">
        <table>
            <tr>
                <th>Month</th><th>Sales</th><th>Salary</th>
            </tr>
            <tr>
                <td>January</td>
                <td class="sales"><?php echo round($month_array[0]); ?></td>
                <td class="salary"><?php echo $salary_array[0]["salary"] ?></td>
            </tr>
            <tr>
                <td>Febuary</td>
                <td class="sales"><?php echo round($month_array[1]); ?></td>
                <td class="salary"><?php echo $salary_array[1]["salary"] ?></td>
            </tr>
            <tr>
                <td>March</td>
                <td class="sales"><?php echo round($month_array[2]); ?></td>
                <td class="salary"><?php echo $salary_array[2]["salary"] ?></td>
            </tr>
            <tr>
                <td>April</td>
                <td class="sales"><?php echo round($month_array[3]); ?></td>
                <td class="salary"><?php echo $salary_array[3]["salary"] ?></td>
            </tr>
            <tr>
                <td>May</td>
                <td class="sales"><?php echo round($month_array[4]); ?></td>
                <td class="salary"><?php echo $salary_array[4]["salary"] ?></td>
            </tr>
            <tr>
                <td>Juni</td>
                <td class="sales"><?php echo round($month_array[5]); ?></td>
                <td class="salary"><?php echo $salary_array[5]["salary"] ?></td>
            </tr>
            <tr>
                <td>July</td>
                <td class="sales"><?php echo round($month_array[6]); ?></td>
                <td class="salary"><?php echo $salary_array[6]["salary"] ?></td>
            </tr>
            <tr>
                <td>August</td>
                <td class="sales"><?php echo round($month_array[7]); ?></td>
                <td class="salary"><?php echo $salary_array[7]["salary"] ?></td>
            </tr>
            <tr>
                <td>September</td>
                <td class="sales"><?php echo round($month_array[8]); ?></td>
                <td class="salary"><?php echo $salary_array[8]["salary"] ?></td>
            </tr>
            <tr>
                <td>October</td>
                <td class="sales"><?php echo round($month_array[9]); ?></td>
                <td class="salary"><?php echo $salary_array[9]["salary"] ?></td>
            </tr>
            <tr>
                <td>November</td>
                <td class="sales"><?php echo round($month_array[10]); ?></td>
                <td class="salary"><?php echo $salary_array[10]["salary"] ?></td>
            </tr>
            <tr>
                <td>December</td>
                <td class="sales"><?php echo round($month_array[11]); ?></td>
                <td class="salary"><?php echo $salary_array[11]["salary"] ?></td>
            </tr>
        </table>
    </article>
       
    </div>



    <article id="day-stats" class="grid-item">
        <div>
            <canvas id="myChart2"></canvas>
        </div>
    </article>

    <article id="month-stats" class="grid-item">
        <div>
            <canvas id="myChart"></canvas>
        </div>
    </article>

  
  </div>

<footer class="flex width-100 justify-content-end ">
<div id="updated" class="flex">
    <div>salary updated: <?php echo $salary_updated; ?></div>
    <div>sales updated: <?php echo $sales_updated; ?></div>
</div>
</footer>

</div>
      

<?php include("footer.php") ?>
<script src="js/Chart.bundle.js"></script>
<script src="js/dashboard.js"></script>

</body>

</html>




