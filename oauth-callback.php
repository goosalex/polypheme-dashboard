<?php
session_start();
?>
<?php
include ("oauth-config.php");

function http($url, $params=false, $token=false) {
    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    if($params)
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($params));
    if($token)
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            // see https://docs.nextcloud.com/server/latest/admin_manual/configuration_server/oauth2.html#the-access-token
            // and https://tools.ietf.org/html/rfc6750#page-5
            "Authorization: Bearer ".$token
        ));
    $p = curl_exec($ch);
    error_log( print_r($p) );
    return json_decode($p);
}
// ********* LOGOUT *****//
if(isset($_GET['logout'])) {
    unset($_SESSION['username'] );

    // we can only logout the User from the Dashboard, but not from nextCloud
    // Assemble URL to send user back to nextCloud, so he can logout there, if needed
    $logout_target = parse_url($authorization_endpoint, PHP_URL_SCHEME)."://";
    $logout_target = $logout_target.parse_url($authorization_endpoint, PHP_URL_HOST);
    if (parse_url($authorization_endpoint, PHP_URL_PORT) != "")
        $logout_target .= ":".parse_url($authorization_endpoint, PHP_URL_PORT);

    header('Location: '.$logout_target);
}
// *************** Callback from nextCloud OAuth2 ************************
if(isset($_GET['code'])) {

    if($_SESSION['state'] != $_GET['state']) {
        die('Authorization server returned an invalid state parameter');
    }

    if(isset($_GET['error'])) {
        die('Authorization server returned an error: '.htmlspecialchars($_GET['error']));
    }

    $token = http($token_endpoint, [
        'grant_type' => 'authorization_code',
        'code' => $_GET['code'],
        'redirect_uri' => $callback_uri,
        'client_id' => $client_id,
        'client_secret' => $client_secret,
    ]);

    if(!isset($token->access_token)) {
        die('Error fetching access token');
    }


    // In case we only need to authorize and don't care about printing the Username, we could stop here.
    // e.g. by setting $_SESSION['username'] = $token->user_id
    // for any API call to get more information, we need to add te access-token to the HTTP Headers when making GET requests.

    $response = http($introspection_endpoint, false,
        $token->access_token
    );

    if($response->ocs->data->{'display-name'} ) {
        $_SESSION['username'] = $response->ocs->data->{'display-name'};
        header('Location: '.dirname($_SERVER['SCRIPT_NAME'])); // should redirect to index.php
        die();
    }
}



?>