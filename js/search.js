$(document).ready(function () {
  $("#search").on("keyup", function () {
    var value;
    value = $(this).val();
    var res = "";

    if (value.includes("&&") === true) {
      res = value.split("&&");
      value = res;
    }

    $("table tr").each(function (index) {
      if (index !== 0) {
        $row = $(this);

        var id = $row.find("td").text().toLowerCase();
        //when // used
        if (value instanceof Array) {
          //https://www.regexpal.com/?fam=116251
          var re = new RegExp("(?=.*" + value[0] + ")(?=.*" + value[1] + ")");
          if (id.search(re) > -1) {
            $row.css("display", "table-row");
          } else {
            $row.css("display", "none");
          }
        } else {
          if (id.includes(value) === false) {
            $row.css("display", "none");
          } else {
            $row.css("display", "table-row");
          }
        }
      }
    });
  });


});
