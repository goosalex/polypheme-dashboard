![icon](images/dachs.png)

# Dashboard
Dashboard for Polyphémes® a bike courier logistics software

# Installation

Copy all files to your webserver with php support
and add a file /config.php with the following content

`
<?php
	$user = "";
    $pass = "";
    $url = "";
    $url_salary = "";
    $url_recurrence ="";
    ?>
`
# dependencies

- PHP
- [jQuery](https://jquery.com/)
- [chart.js](https://www.chartjs.org/)
- [masonry](https://masonry.desandro.com/)



# To-Do

- PDF download
- Ranklist best customer
- dummy data.json
