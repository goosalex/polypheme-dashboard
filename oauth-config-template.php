<?php
// Please set the following parameters and rename the file to "oauth-config.php"

// Following 3 parameters should exactly match what is configured in nextCloud / Security

$client_id = '77eG13ltrIb01iLJzaOdG1q6F4c9A6WKLNfCAcPsNeHHYLwYNJa4VrD2iPtsq1aI'; // The client ID assigned to you by the provider
$client_secret = '2yws438OItPB1grFeECNBoYXWqKaJtctvGpfq94gwaYJ8JzTVn1Ifvah71KTd5h7';  // The client secret assigned to you by the provider
$callback_uri = 'http://localhost:63340/polypheme-dashboard/oauth-callback.php';  // Callback URL for OAuth2 server to redirect to. It must match your machine/installation exactly

// in the following three configs, replace https://cloud.tabido.li with your extCloud URL

// Step 1: Redirection to Login Page.
$authorization_endpoint = 'http://cloud.tabido.li/index.php/apps/oauth2/authorize'; // Authorization URL

// Step 2: URL to get AccessToken: - Synchronous call.
$token_endpoint = 'http://cloud.tabido.li/index.php/apps/oauth2/api/v1/token'; // Token URL

// Step 3: URL to get User Infos - Synchronous call.
$introspection_endpoint = 'http://cloud.tabido.li/ocs/v2.php/cloud/user?format=json'; //
?>
