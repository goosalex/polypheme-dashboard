<?php include ("oauth-session.php") ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="icon" type="image/png" href="favicon.png">
    <meta charset="utf-8">
    <meta name="description" content="Dashboard">
    <title>Dashboard</title>
    <link rel="stylesheet" href="css/main.css">
    <link rel="stylesheet" href="css/grid.css">

    <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">
</head>




<body id="recurrence">


<div id='wrapper'>
<?php include("navigation.php") ?>
    <div id="toast"></div>

    <?php

    //get url/user/pw
    require("config.php");
 
    ///////////////////
    //fetch data
    ///////////////////


    
        $context = stream_context_create(array(
            'http' => array(
                'header'  => "Authorization: Basic " . base64_encode($user.":".$pass)
            )
        ));
        $data = file_get_contents($url_recurrence,true,$context);
        $json_a = json_decode($data,true);


        $data_array =[];

        foreach($json_a as $key=>$value):
        
            $status = $json_a[$key]["meta.status"];
            $info = $json_a[$key]["meta.summaries.info"];
            $customer = $json_a[$key]["billingCustomerId"];
            $orderedBy = $json_a[$key]["orderedByCustomerId"];
            $startTime = $json_a[$key]["meta.summaries.startTime"];
            $endTime = $json_a[$key]["meta.summaries.endTime"];
            $enabled = $json_a[$key]["recurrenceEnabled"];
            $title = $json_a[$key]["reference"];
            $favorite = $json_a[$key]["favorite"];

            if( $favorite  == "false"):

                //push in array to sort 
                if(!is_numeric($startTime) ):
                    $startTime = strtotime($startTime);
                    $startTime = date("H:i", $startTime);
                endif;

                if(!is_numeric($endTime) ):
                    $endTime = strtotime($endTime);
                    $endTime = date("H:i", $endTime);
                endif;

                if($endTime == "01:00"):
                    $endTime = 0;
                endif;


                if($startTime == "01:00"):
                    $startTime = 0;
                endif;



                    array_push($data_array,[
                        
                        "status"=>$status,
                        "orderedBy"=>$orderedBy,
                        "startTime"=>$startTime,
                        "endTime"=>$endTime,
                        "title"=>$title,
                        "info"=>$info,
                        "customer"=>$customer
                        ]);
            

        
            endif;
        endforeach;


        //before getting sum per day sort array by date
        function mySort($a,$b) {
            
                return $a['startTime'] - $b['startTime'];
            
        }
        usort($data_array, 'mySort'); 

?>

   


    <table>
        <thead>
            <tr>
                <th>Start</th>
                <th>End</th>
                <th>Customer</th>
                <th>Mission</th>
                <th>Status</th>
            </tr>
        </thead>
        
        <?php 
        foreach($data_array as $key=>$value):
        

            $title = $data_array[$key]["title"];
            $customer = $data_array[$key]["customer"];
            $status = $data_array[$key]["status"];
            $info = $data_array[$key]["info"];
            $startTime = $data_array[$key]["startTime"];
            $endTime = $data_array[$key]["endTime"];
            $enabled = $data_array[$key]["recurrenceEnabled"];

            if($enabled = true):
                $enabled = "enabled";
            else:
                $enabled = "disabled";
            endif;



            echo "<tr class=".$enabled.">";
            echo "<td>".$startTime."</td>";
            echo "<td>".$endTime."</td>"; 
            echo "<td>".$customer."</td>";
            echo "<td>".$info."</td>";
            echo "<td>".$status."</td>";
            echo "<tr>";

        endforeach;
        ?>
    </div>



  
  <?php include("footer.php") ?>

    

</div>
      


</body>

</html>




