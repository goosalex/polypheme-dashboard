<?php include ("oauth-session.php") ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="icon" type="image/png" href="favicon.png">
    <meta charset="utf-8">
    <meta name="description" content="Dashboard">
    <title>Dashboard</title>
    <link rel="stylesheet" href="css/main.css">
    <link rel="stylesheet" href="css/grid.css">
    <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">
</head>




<body id="preorder">


  <div id='wrapper' class="grid">
    <?php include("navigation.php") ?>
    <div id="toast"></div>

    <?php

    //get url/user/pw
    require("config.php");

    $sales_updated = "--";

    
    ///////////////////
    //fetch data sales
    ///////////////////

    
    $context = stream_context_create(array(
        'http' => array(
            'header'  => "Authorization: Basic " . base64_encode($user.":".$pass)
        )
    ));
    $data = file_get_contents($url,true,$context);
    $json_a = json_decode($data,true);



    $newDate = new DateTime();
    $now = $newDate->getTimestamp(); 

    $today = $newDate->format('d.m.Y');
    $today_month = $newDate->format('n');
    $day = "";


    $total_year;
    $month_array =[0,0,0,0,0,0,0,0,0,0,0,0];
    $data_array =[[
            "timestamp"=>0 ,
            "date"=>0,
            "datetime"=>0,
            "sum"=>0,
            "reference"=>0,
            "stop"=>0,
            "orderedBy"=>0,
            "pick"=>0,
            "drop"=>0,
            "status"=>0
    ]
    ];




    foreach($json_a as $key=>$value)
    {
        $startDate_unix = strtotime($json_a[$key]["meta.summaries.startDate"]);
        $date = new DateTime($json_a[$key]["meta.summaries.startDate"]);
        $date_before = new DateTime($json_a[$key-1]["meta.summaries.startDate"]);
        $datetime = new DateTime($json_a[$key]["meta.summaries.startDate"].$json_a[$key]["meta.summaries.startTime"] );
        $reference = $json_a[$key]["reference"];
        $pick = $json_a[$key]["meta.summaries.startShortInfo"];
        $drop = $json_a[$key]["meta.summaries.endShortInfo"];
        $orderedBy = $json_a[$key]["orderedByCustomerId"];
        $reference = $json_a[$key]["reference"];
        $status =  $json_a[$key]["status"];
        $price = $json_a[$key]["sums.base"];
        $status = $json_a[$key]["status"];
        $isRecurrent = $json_a[$key]["isRecurrent"];
        //$stop = $json_a[$key]["meta.summaries.stepsShortInfo"];



        if($status == "ordered" && $isRecurrent === false)
        {

        //push in array to sort 

            array_push($data_array,[
                "timestamp"=> $startDate_unix ,
                "weekday"=> $date->format('N'),
                "date"=>$date->format('d.m.Y'),
                "datetime"=>$datetime,
                "sum"=>$price,
                "reference"=>$id,
                "stop"=>$stop,
                "orderedBy"=>$orderedBy,
                "pick"=>$pick,
                "drop"=>$drop,
                "status"=>$status
                ]);

            }



    }


    //before getting sum per day sort array by date
    function mySort($a,$b) {
        
            return $a['timestamp'] - $b['timestamp'];
        
    }

    usort($data_array, 'mySort'); 


    
    //echo '<pre>';
    //echo print_r($data_array);
    //echo '<pre>';
    
    


?>



<div id="preorder" class="flex width-100">
        <table>
            <tr>
                <th class="grid-col-25">Monday</th>
                <th class="grid-col-25">Tuesday</th>
                <th class="grid-col-25">Wednesday</th>
                <th class="grid-col-25">Thursday</th>
                <th class="grid-col-25">Friday</th>
                <th class="grid-col-25">Saturday</th>
                <th class="grid-col-25">Sunday</th>
            </tr>
        <?php 
        foreach($data_array as $key=>$value):
        
            $datetime = $data_array[$key]["datetime"];
            $weekday = $data_array[$key]["weekday"];
            $timestamp = $data_array[$key]["timestamp"];
            $status = $data_array[$key]["status"];
            $pick = $data_array[$key]["pick"];
            $drop = $data_array[$key]["drop"];
            $stop = $data_array[$key]["stop"];
            $reference = $data_array[$key]["reference"];
            $orderedBy = $data_array[$key]["orderedBy"];

            if($status == "ordered" && $now-86400 < $timestamp):
            
                echo "<tr>";
                for($i = 1; $i < 8; $i++)
                {
                echo "<td>";
                if($weekday == $i):
                
                    echo "<article class='".$day."' data-datetime=".$startDate_unix.">";
                    echo "<h1 class='date'>".$datetime->format('d.m.Y, H:i')."</h1>";
                    echo "<div><span>Pick</span> ".$pick."</div>";
                    if($stop)
                    {
                        echo "<div><span>Stopps</span> ".$stop."</div>";

                    }
                    echo "<div><span>Drop</span>".$drop."</div>";
                    echo "<div class='orderby'><span>OrderBy</span>".$orderedBy."</div>";
                    echo "<div class='id' data-id='.$reference.'>ID</div>";;
                    echo "</article>";

                endif;
                echo "</td>";
            }
               



              

               
                echo "</tr>";

            endif;

        endforeach;


        ?>
        </table>
        </div>





        <?php include("footer.php") ?>

    

</div>
      


  
 


</body>

</html>




