<?php include ("oauth-session.php") ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="icon" type="image/png" href="favicon.png">
    <meta charset="utf-8">
    <meta name="description" content="Dashboard">
    <title>Dashboard</title>
    <link rel="stylesheet" href="css/main.css">
    <link rel="stylesheet" href="css/grid.css">
    <link rel="stylesheet" href="css/jquery.datetimepicker.min.css">

    <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">
</head>




<body id="customers">

  <div id='wrapper'>
    <?php include("navigation.php") ?>
    <div id="toast"></div>
    <div id="top-bar" class="flex">
        <input type="search" id="search" class="grid-col-35" placeholder="search terms can be combined with &&"></input>
        <div id="download"><img class="grid-col-3" src="download.png"></div>
    </div>
    <?php

    //get url/user/pw
    require("config.php");


    
    ///////////////////
    //fetch customers
    ///////////////////

    
    $context = stream_context_create(array(
        'http' => array(
            'header'  => "Authorization: Basic " . base64_encode($user.":".$pass)
        )
    ));
    $data = file_get_contents($url_customers,true,$context);
    $json_a = json_decode($data,true);




    $data_array =[
    ];




    foreach($json_a as $key=>$value)

    {
        array_push($data_array,[
            "type"=> $json_a[$key]["type"],
            "alias"=>$json_a[$key]["alias"],
            "companyName"=>$json_a[$key]["companyName"],
            "lastName"=>$json_a[$key]["lastName"],
            "firstName"=>$json_a[$key]["firstName"],
            "places[0].zip"=>$json_a[$key]["places[0].zip"],
            "places[0].locality"=>$json_a[$key]["places[0].locality"],
            "places[0].street"=>$json_a[$key]["places[0].street"],
            "places[0].houseNumber"=>$json_a[$key]["places[0].houseNumber"],
            "places[0].zipId"=>$json_a[$key]["places[0].zipId"],
            "contactMeans[0].value"=>$json_a[$key]["contactMeans[0].value"],
            "contactMeans[1].value"=>$json_a[$key]["contactMeans[1].value"],
            "notes[0].content"=>$json_a[$key]["notes[0].content"],
            "notes[1].content"=>$json_a[$key]["notes[1].content"],
            "invoice"=>$json_a[$key]["invoiceEmail"],  
            "meta.status"=>$json_a[$key]["meta.status"]
           
            ]);
       

    }

    //before getting sum per day sort array by date
    function mySort($a,$b) {
        
            return $a['type'] - $b['type'];
        
    }
    //usort($data_array, 'mySort'); 

    /*
    echo '<pre>';
    echo print_r($data_array);
    echo '<pre>';
    */
    
    

?>



<table>
<thead>
    <tr>
        <th><span>Type</span><span>......</span></th>
        <th><span>Company</span><span>.....</span></th>
        <th><span>Lastname</span><span>.....</span></th>
        <th><span>Firstname</span><span>.....</span></th>
        <th><span>Zip</span><span>.....</span></th>
        <th><span>Location</span><span>.....</span></th>
        <th><span>Street</span><span>.....</span></th>
        <th><span>Number</span><span>.....</span></th>
        <th><span>Contact</span><span>....</span></th>
        <th><span>Contact</span><span>.....</span></th>
        <th><span>Invoice</span><span>.....</span></th>
        <th><span>Status</span><span>.....</span></th>
    </tr>
<thead>

        <?php 
        foreach($data_array as $key=>$value)
        {
            $contact_1 = $data_array[$key]["contactMeans[1].value"];
            //check if email
            if (filter_var($data_array[$key]["contactMeans[1].value"], FILTER_VALIDATE_EMAIL)) {
                $contact_1 = '<a href="mailto:'.$data_array[$key]["contactMeans[1].value"].'">'.$data_array[$key]["contactMeans[1].value"].'</a>';
              }
                   //check if tel number
                   if(preg_match("/[0-9]+/", $contact_1)) {
                    $contact_1 = '<a href="tel:'.$data_array[$key]["contactMeans[1].value"].'">'.$data_array[$key]["contactMeans[1].value"].'</a>';
                }
          

              $contact_0 = $data_array[$key]["contactMeans[0].value"];
              //check if email
              if (filter_var($data_array[$key]["contactMeans[0].value"], FILTER_VALIDATE_EMAIL)) {
                  $contact_0 = '<a href="mailto:'.$data_array[$key]["contactMeans[0].value"].'">'.$data_array[$key]["contactMeans[0].value"].'</a>';
                }

                
                 //check if tel number
              if(preg_match("/[0-9]+/", $contact_0)) {
                $contact_0 = '<a href="tel:'.$data_array[$key]["contactMeans[0].value"].'">'.$data_array[$key]["contactMeans[0].value"].'</a>';
            }


                     
                echo "<tr>";
                echo "<td>".$data_array[$key]['type']."</td>";
                echo "<td>".$data_array[$key]["companyName"]."</td>";
                echo "<td>".$data_array[$key]["lastName"]."</td>";
                echo "<td>".$data_array[$key]["firstName"]."</td>";
                echo "<td>".$data_array[$key]["places[0].zip"]."</td>";
                echo "<td>".$data_array[$key]["places[0].locality"]."</td>";
                echo "<td>".$data_array[$key]["places[0].street"]."</td>";
                echo "<td>".$data_array[$key]["places[0].houseNumber"]."</td>";
                echo "<td>".$contact_0."</td>";
                echo "<td>".$contact_1."</td>";
                echo "<td>".$data_array[$key]["invoice"]."</td>";
                echo "<td>".$data_array[$key]["meta.status"]."</td>";
                echo "</tr>";
                 
    }


        ?>
        </table>





        <?php include("footer.php") ?>

    

</div>      





</body>

</html>




