$(document).ready(function() {












    function addChart() {
        let month_data = [];
        let month_data_salary = [];
        $('article#month table td.sales').each(function(i) {
            month_data.push($(this).text());
        });

        $('article#month table td.salary').each(function(i) {
            month_data_salary.push($(this).text());
        });


        var ctx = document.getElementById('myChart');


        var myChart = new Chart(ctx, {
            type: 'horizontalBar',
            data: {
                labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],

                datasets: [{
                        label: 'Monthly sales',
                        data: month_data,
                        backgroundColor: 'white'
                    },
                    {
                        label: 'Monthly salary',
                        data: month_data_salary,
                        backgroundColor: 'red'
                    }
                ]
            },
            options: {
                legend: {
                    labels: {
                        // This more specific font property overrides the global property
                        fontColor: 'white'
                    }
                },
                scales: {
                    yAxes: [{
                        ticks: {
                            fontColor: "white",
                            beginAtZero: true
                        }
                    }],
                    xAxes: [{
                        ticks: {
                            fontColor: "white",
                            beginAtZero: true
                        }
                    }]
                }
            }
        });


        //////////////////////////
        //each day///////////////
        /////////////////////////

        let days_data = [];
        let days = [];
        $('article#days div').each(function(i) {
            days_data.push($(this).contents().eq(1).text());
            days.push($(this).contents().eq(0).text());
        });

        var ctx2 = document.getElementById('myChart2');


        var myChart2 = new Chart(ctx2, {
            type: 'line',
            data: {
                labels: days,

                datasets: [{
                    label: 'Daily sales',
                    data: days_data,
                    backgroundColor: 'rgba(0,40,0,0.5)',
                    borderColor: 'white'


                }]
            },
            options: {
                legend: {
                    labels: {
                        // This more specific font property overrides the global property
                        fontColor: 'white'
                    }
                },
                scales: {
                    yAxes: [{
                        ticks: {
                            fontColor: "white",
                            beginAtZero: true,


                        }
                    }],
                    xAxes: [{
                        ticks: {
                            fontColor: "dimgray",
                            beginAtZero: true,

                        }
                    }]
                }
            }
        });


    }


    addChart();




})