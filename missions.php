<?php include ("oauth-session.php") ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="icon" type="image/png" href="favicon.png">
    <meta charset="utf-8">
    <meta name="description" content="Dashboard">
    <title>Dashboard</title>
    <link rel="stylesheet" href="css/main.css">
    <link rel="stylesheet" href="css/grid.css">
    <link rel="stylesheet" href="css/jquery.datetimepicker.min.css">

    <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">
</head>




<body id="missions">

  <div id='wrapper'>
    <?php include("navigation.php") ?>
    <div id="toast"></div>
    <div id="top-bar">
        <div class="flex grid-col-50 justify-content-spacebetween">
            <label for="search">Search</label>
            <input type="search" name="search" id="search" class="grid-col-35" placeholder="search terms can be combined with &&"></input>
        </div>

        <div class="flex grid-col-50 justify-content-spacebetween">
            <label for="meta-status">Meta</label>
            <input type="text" name="meta-status" id="meta-status" class="grid-col-35" placeholder="meta.status"></input>
        </div>

        <div class="flex grid-col-50 justify-content-spacebetween">
            <label for="status">Status</label>
            <input type="text" name="status" id="status" class="grid-col-35" placeholder="status"></input>
        </div>


        <div class="flex grid-col-50 justify-content-spacebetween">
            <label for="customer">Customer</label>
            <input type="text" id="customer" name="customer" class="grid-col-35" placeholder="customer" autocomplete="off"></input>
        </div>

        <div class="flex grid-col-50 justify-content-spacebetween">
            <label for="mate">Mate</label>
            <input type="text" id="mate" name="mate" class="grid-col-35" placeholder="mate" autocomplete="off"></input>
        </div>

        
        <div class="flex grid-col-50 justify-content-spacebetween">
            <label for="from">From</label>
            <input type="text" id="from"   name="from"  class="datetime-picker grid-col-35" placeholder="from"></input>
        </div>
        <div class="flex grid-col-50 justify-content-spacebetween">
            <label for="to">To</label>
            <input type="text" id="to" name="to" class="datetime-picker grid-col-35" placeholder="to"></input>
        </div>

        <div id="download"><img class="grid-col-3" src="download.png"></div>

    </div>
    <?php

    //get url/user/pw
    require("config.php");


    
    ///////////////////
    //fetch customers
    ///////////////////

    
    $context = stream_context_create(array(
        'http' => array(
            'header'  => "Authorization: Basic " . base64_encode($user.":".$pass)
        )
    ));
    $data = file_get_contents($url,true,$context);
    $json_a = json_decode($data,true);




    $data_array =[];



    foreach($json_a as $key=>$value)
    {
        $product = [];
        $products = "";


        //unix timestamp
        $dateF = "";
        date_default_timezone_set("UTC");

        if($json_a[$key]["meta.summaries.startDate"] != "")
        {
            $dateF = date("d.m.Y",strtotime($json_a[$key]["meta.summaries.startDate"]));
        }
        //push & merge products
        for($i = 0; $i < 15; $i++)
        {
            if($json_a[$key]["positions[".$i."].meta.summaries.info"] != "")
            {
                array_push($product,
            $json_a[$key]["positions[".$i."].meta.summaries.info"]
            );

            }
            
        }


        $products = join("<br>",$product);





            array_push($data_array,[
                "timestamp"=> strtotime($dateF),
                "date"=> $dateF,
                "time"=>$json_a[$key]["meta.summaries.startTime"],
                "pick"=>$json_a[$key]["meta.summaries.startShortInfo"],
                "drop"=>$json_a[$key]["meta.summaries.endShortInfo"],
                "billingCustomer"=>$json_a[$key]["billingCustomerId"],
                "mate"=>$json_a[$key]["realisedBy"],
                "stops"=>$json_a[$key]["stepsInfo"],
                "price"=>$json_a[$key]["sums.base"],
                "products"=>$products,
                "meta.status"=>$json_a[$key]["meta.status"],
                "status"=>$json_a[$key]["status"],

                ]);
    }


  
    //sort array by date
    uasort($data_array, function($a, $b) {
        return strcmp($a['timestamp'], $b['timestamp']);
    });

    /*
    echo '<pre>';
    echo print_r($data_array);
    echo '<pre>';
    */
    
    


?>



<table>
<thead>
    <tr>
        <th><span>Timestamp</span><span>...</span></th>
        <th><span>Date</span><span>...</span></th>
        <th><span>Time</span><span>...</span></th>
        <th><span>Pick</span><span>...</span></th>
        <th><span>Drop</span><span>...</span></th>
        <th><span>Stops</span><span>...</span></th>
        <th><span>Billing</span><span>...</span></th>
        <th><span>Mate</span><span>...</span></th>
        <th><span>Price</span><span>...</span></th>
        <th><span>Products</span><span>...</span></th>
        <th><span>Meta.Status</span><span>...</span></th>
        <th><span>Status</span><span>...</span></th>
    </tr>
</thead>

        <?php 
        foreach($data_array as $key=>$value)
        {
      
                echo "<tr>";
                echo "<td>".$data_array[$key]['timestamp']."</td>";
                echo "<td>".$data_array[$key]['date']."</td>";
                echo "<td>".$data_array[$key]["time"]."</td>";
                echo "<td>".$data_array[$key]["pick"]."</td>";
                echo "<td>".$data_array[$key]["drop"]."</td>";
                echo "<td>".$data_array[$key]["stops"]."</td>";
                echo "<td>".$data_array[$key]["billingCustomer"]."</td>";
                echo "<td>".$data_array[$key]["mate"]."</td>";
                echo "<td>".$data_array[$key]["price"]."</td>";
                echo "<td>".$data_array[$key]["products"]."</td>";
                echo "<td>".$data_array[$key]["meta.status"]."</td>";
                echo "<td>".$data_array[$key]["status"]."</td>";
              
                echo "</tr>";
            
        
    }


        ?>
        </table>





        <?php include("footer.php") ?>
        <script src="js/missions.js"></script>

    

</div>      




</body>

</html>




