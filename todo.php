<?php include ("oauth-session.php") ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="icon" type="image/png" href="favicon.png">
    <meta charset="utf-8">
    <meta name="description" content="Dashboard">
    <title>Dashboard</title>
    <link rel="stylesheet" href="css/main.css">
    <link rel="stylesheet" href="css/grid.css">
    <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">
</head>




<body id="todo">

  <div id='wrapper' class="flex justify-content-spacearound">
    <?php include("navigation.php") ?>
    <div id="toast"></div>
    <div id="content" class="grid-col-60">
 
        <?php

        //get url/user/pw
        require("config.php");
        ///////////////////
        //fetch customers
        ///////////////////
        $context = stream_context_create(array(
            'http' => array(
                'header'  => "Authorization: Basic " . base64_encode($user.":".$pass)
            )
        ));
        $data = file_get_contents($url,true,$context);
        $json_a = json_decode($data,true);

        $data_array =[];

        $newDate = new DateTime();
        $now = $newDate->getTimestamp(); 
        $today = date("d.m.Y",time());

        echo $today;
        $filter_mate = htmlspecialchars($_GET["mate"]);

        echo $filter_mate;
        ?>

        <?php if(empty($filter_mate)): ?>
            <div id="top-bar">
                <div class="flex grid-col-60 justify-content-spacebetween">
                    <input type="search" name="mate" id="mate" class="grid-col-60" placeholder="mate"></input>
                </div>
            </div>
        <?php endif; ?>


<?php

        foreach($json_a as $key=>$value)
        {


            $product = [];
            $products = "";
            //unix timestamp
            $dateF = "";
            $date ="";
            if($json_a[$key]["meta.summaries.startDate"] != "")
            {
                $dateF = date("d.m.Y",strtotime($json_a[$key]["meta.summaries.startDate"]));
            }

            
            //push & merge products
            for($i = 0; $i < 15; $i++)
            {
                if($json_a[$key]["positions[".$i."].meta.summaries.info"] != "")
                {
                    array_push($product,
                $json_a[$key]["positions[".$i."].meta.summaries.info"]
                );

                }  
            }



            if($filter_mate != ""):
                if($json_a[$key]["realisedBy"] === $filter_mate && $dateF == $today):
                $products = join("<br>",$product);

                    array_push($data_array,[
                        "timestamp"=> strtotime($dateF),
                        "date"=> $dateF,
                        "time"=>$json_a[$key]["meta.summaries.startTime"],
                        "pick"=>$json_a[$key]["meta.summaries.startShortInfo"],
                        "drop"=>$json_a[$key]["meta.summaries.endShortInfo"],
                        "billingCustomer"=>$json_a[$key]["billingCustomerId"],
                        "mate"=>$json_a[$key]["realisedBy"],
                        "stops"=>$json_a[$key]["stepsInfo"],
                        "price"=>$json_a[$key]["sums.base"],
                        "products"=>$products,
                        "meta.status"=>$json_a[$key]["meta.status"],
                        "status"=>$json_a[$key]["status"]

                        ]);
                endif; 
            endif;

            if(empty($filter_mate)):
                if($dateF == $today):
                $products = join("<br>",$product);

                    array_push($data_array,[
                        "timestamp"=> strtotime($dateF),
                        "date"=> $dateF,
                        "time"=>$json_a[$key]["meta.summaries.startTime"],
                        "pick"=>$json_a[$key]["meta.summaries.startShortInfo"],
                        "drop"=>$json_a[$key]["meta.summaries.endShortInfo"],
                        "billingCustomer"=>$json_a[$key]["billingCustomerId"],
                        "mate"=>$json_a[$key]["realisedBy"],
                        "stops"=>$json_a[$key]["stepsInfo"],
                        "price"=>$json_a[$key]["sums.base"],
                        "products"=>$products,
                        "meta.status"=>$json_a[$key]["meta.status"],
                        "status"=>$json_a[$key]["status"]

                        ]);
                endif; 
            endif;

            
        }


    
                //sort array by date
                uasort($data_array, function($a, $b) {
                    return strcmp($a['time'], $b['time']);
                });

                /*
                echo '<pre>';
                print_r($data_array);
                echo '<pre>';
                */
        




                foreach($data_array as $key=>$value)
                {
                    if($data_array[$key]['status'] == "ordered" || $data_array[$key]['status'] == "produced"):
                        if($data_array[$key]['meta.status'] == "published"):

                            echo "<article class='grid-col-60'>";
                            echo "<div class='timestamp'>".$data_array[$key]['timestamp']."</div>";
                            echo "<div class='date'>".$data_array[$key]['date']."</div>";
                            echo "<div class='time'>".$data_array[$key]['time']."</div>";
                            echo "<div class='pick'>".$data_array[$key]['pick']."</div>";
                            echo "<div class='drop'>".$data_array[$key]['drop']."</div>";
                            echo "<div class='stops'>".$data_array[$key]['stops']."</div>";
                            echo "<div class='price'>".$data_array[$key]['price']."</div>";
                            echo "<div class='status'>".$data_array[$key]['status']."</div>";
                            echo "<div class='status'>".$data_array[$key]['meta.status']."</div>";
                            echo "<div class='mate'>".$data_array[$key]['mate']."</div>";

                            echo "</article>";

                        endif;
                    endif;
                
                }


            ?>


            <?php include("footer.php") ?>
            <script src="js/todo.js"></script>


        </div>      

    </div>

</body>

</html>




