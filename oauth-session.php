<?php session_start();

if(file_exists('oauth-config.php')):
    include ("oauth-config.php");
endif;

// *************************** UNAUTHENTICATED ************************3
// If there is no username, they are logged out, so show them the login link

$authoriation_active = false;

if($authoriation_active && !isset($_SESSION['username'])) {

    $_SESSION['state'] = bin2hex(random_bytes(5)); // Random value to prevent CSRF (they say)

    $authorize_url = $authorization_endpoint . '?' . http_build_query([
            'response_type' => 'code',
            'client_id' => $client_id,
            'redirect_uri' => $callback_uri,
            'state' => $_SESSION['state'],
            'scope' => 'openid',
        ]);

    header('Location: '.$authorize_url);
}
?>