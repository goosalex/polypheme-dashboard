<nav>
	<input type="checkbox" id="desktop-icon" />
	<label for="desktop-icon" class="grid-col-30">
	<div id="menu-button">
		<div id="open-button">
			MENU
		</div>
	</div>




	</label>

	<div id="navigation-content" class="flex width-100">
	<div id="logo"><img src="images/dachs-white.png"></div>
		<div class="inner flex width-100 justify-content-spacearound">
			<ul class="grid-col-50">
				<li><a  <?php if( basename($_SERVER['SCRIPT_NAME'])=="index.php" ||  basename($_SERVER['SCRIPT_NAME'])=="")   { echo "class=\"active\""; } ?> href="index.php">Statistik</a></li>
				<li><a  <?php if( basename($_SERVER['SCRIPT_NAME'])=="recurrence.php")   { echo " class=\"active\""; } ?> href="recurrence.php">Recurrence</a></li>
				<li><a  <?php if( basename($_SERVER['SCRIPT_NAME'])=="preorder.php")   { echo " class=\"active\""; } ?> href="preorder.php">Preorder</a></li>
				<li><a  <?php if( basename($_SERVER['SCRIPT_NAME'])=="customers.php")   { echo " class=\"active\""; } ?> href="customers.php">Customers</a></li>
				<li><a  <?php if( basename($_SERVER['SCRIPT_NAME'])=="missions.php")   { echo " class=\"active\""; } ?> href="missions.php">Missions</a></li>
				<li><a  <?php if( basename($_SERVER['SCRIPT_NAME'])=="todo.php")   { echo " class=\"active\""; } ?> href="todo.php">ToDo</a></li>
                <li><a href="oauth-callback.php?logout=true">Logout <?php echo $_SESSION['username']?></a></li>
			</ul>
		</div>
	</div>
</nav>