//autocomplete
var mate_list = [];
var customers_list = [];
var date_list = [];
var status_list = [];
var meta_status_list = [];



function create_list() {
    $('table#missions td:nth-child(8)').each(function() {
        if (mate_list.some(meta_status_list => meta_status_list.value === $(this).text())) {
            //console.log("Object found inside the array.");
        } else {
            mate_list.push({ "value": $(this).text(), "data": $(this).text() });
        }
    });



    $('table#missions td:nth-child(7)').each(function() {
        if (customers_list.some(meta_status_list => meta_status_list.value === $(this).text())) {
            //console.log("Object found inside the array.");
        } else {
            customers_list.push({ "value": $(this).text(), "data": $(this).text() });
        }
    });

    $('table#missions td:nth-child(1)').each(function() {
        if (date_list.some(meta_status_list => meta_status_list.value === $(this).text())) {
            //console.log("Object found inside the array.");
        } else {
            date_list.push({ "value": $(this).text(), "data": $(this).text() });
        }
    });


    $('table#missions td:nth-child(12)').each(function() {
        if (status_list.some(meta_status_list => meta_status_list.value === $(this).text())) {
            //console.log("Object found inside the array.");
        } else {
            status_list.push({ "value": $(this).text(), "data": $(this).text() });
        }
    });

    $('table#missions td:nth-child(11)').each(function() {
        if ($(this).text() != "") {
            if (meta_status_list.some(meta_status_list => meta_status_list.value === $(this).text())) {
                //console.log("Object found inside the array.");
            } else {
                meta_status_list.push({ "value": $(this).text(), "data": $(this).text() });
            }

        }
    });


}

create_list()


function buildAutocomplete(element, source, param) {
    $(element).autocomplete({
        lookup: source,
        minChars: 1,
        triggerSelectOnValidInput: false,
        showNoSuggestionNotice: true,
        lookupLimit: 5,

        onSearchStart: function() {
            //alert("search start")

        },
        onSearchError: function(query, jqXHR, textStatus, errorThrown) {
            //alert(query)
        },
        onSelect: function(suggestion, onselect) {
            param()

            lang = suggestion.data;
            $('element').autocomplete('disable');

        },

        onSearchComplete: function(query, suggestions) {

            //alert(query.length)


        }
    });
}

buildAutocomplete("#mate", mate_list)
buildAutocomplete("#customer", customers_list, customer_filter)
buildAutocomplete("#status", status_list)
buildAutocomplete("#meta-status", meta_status_list)





//missions filter
$("#status").on("keyup", function() {
    var value;
    value = $(this).val();


    $("table tr:visible").each(function(index) {
        if (index !== 0) {
            $row = $(this);

            var id = $row.find("td:nth-child(12)").text();


            if (id.indexOf(value) === -1) {
                $row.css("display", "none");
            } else {
                $row.css("display", "table-row");
            }

        }
    });

    reset_search()
});


$("#meta-status").on("keyup", function() {
    var value;
    value = $(this).val();


    $("table tr:visible").each(function(index) {
        if (index !== 0) {
            $row = $(this);

            var id = $row.find("td:nth-child(11)").text();


            if (id.indexOf(value) === -1) {
                $row.css("display", "none");
            } else {
                $row.css("display", "table-row");
            }

        }
    });

    reset_search()
});




$("#mate").on("keyup", function() {
    var value;
    value = $(this).val();


    $("table tr:visible").each(function(index) {
        if (index !== 0) {
            $row = $(this);

            var id = $row.find("td:nth-child(8)").text();


            if (id.indexOf(value) === -1) {
                $row.css("display", "none");
            } else {
                $row.css("display", "table-row");
            }

        }
    });

    reset_search()
});




function customer_filter() {
    var value = $("#customer").val();

    $("table tr:visible").each(function(index) {
        if (index !== 0) {
            $row = $(this);

            var id = $row.find("td:nth-child(7)").text()
                //console.log(id + "/" + value)
            if (id.indexOf(value) != -1) {
                $row.css("display", "table-row");
            } else {
                $row.css("display", "none");
            }

        }
    });

    reset_search()
}







//date range filterfilter 
function date_range() {

    $("table tr:visible").each(function(index) {
        if (index !== 0) {
            $row = $(this);

            var id = $row.find("td:nth-child(1)").text();

            var val_from = new Date(document.getElementById("from").value)
                //var from = moment.utc(val_from).unix();

            var from = Date.parse(val_from) / 1000

            var val_to = $("#to").val()
                //var to = moment.utc(val_to).unix();

            var to = Date.parse(val_to) / 1000

            console.log(from + "/" + to)
            if (id >= from && id <= to) {

                $row.css("display", "table-row");

            } else {
                $row.css("display", "none");

            }

        }
    })


    reset_search()

};


function reset_search() {
    if (!$("#customer").val() &&
        !$("#mate").val() &&
        !$("#meta-status").val() &&
        !$("#status").val()
    ) {
        console.log("yeah")
        $("table tr").css("display", "table-row");
    }
}




//timepicker
$('.datetime-picker').datetimepicker({
    format: 'Y-m-d',
    openOnFocus: true,
    timepicker: false,
    timepickerScrollbar: false,
    todayButton: true,
    lang: 'de',
    onSelectDate: function(ct, $i) {
        setTimeout(() => {
            $("input:focus").is("input#to") ? date_range() : "";

        }, 1000);


    }
});